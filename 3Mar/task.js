/**
 * It prints a square of asterisks.
 * @param {number} panjang - the number of times the loop will run.
 * @returns {string} the square of asterisks.
 */
const kotak = (panjang) => {
    let hasil = '';
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j < panjang; j++) {
            hasil += '* ';
        }
        hasil += '\n';
    }
    return hasil;
}

console.log(kotak(5));

/**
 * It prints a triangle of asterisks.
 * @param {number} panjang - the number of rows to print
 * @returns {string} the triangle of asterisks.
 */
const segitiga = (panjang) => {
    let hasil = '';
    for (let i = 0; i < panjang; i++) {
        /* It's a nested loop. */
        for (let j = 0; j <= i; j++) {
            hasil += '* ';
        }
        hasil += '\n';
    }
    return hasil;
}

console.log(segitiga(5));

/**
 * Given a number, return the nth Fibonacci number
 * @param {number} num - The number of times to run the loop.
 * @returns {number} The nth Fibonacci number.
 */
const fibonacci = (num) => {
    let a = 1, b = 0, temp;
    while (num > 0) {
        temp = a;
        a = a + b;
        b = temp;
        num--;
    }
    return b;
}

console.log(fibonacci(10));

/**
 * It returns an array of fibonacci series
 * @param {number} num - the number of elements in the series.
 * @returns {number[]} An array of numbers.
 */
const fibonacciSeries = (num) => {
    let hasil = [];
    for (let i = 0; i <= num; i++) {
        hasil.push(fibonacci(i));
    }
    return hasil;
}

console.log(fibonacciSeries(10));
