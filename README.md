# Binar Academy - Chapter 1

What I learn in Binar Academy Backend JavaScript course Chappter 1:

### Table of Contents

1. [25 Feb](#25-feb)
2. [28 Feb](#28-feb)
3. [1 Mar](#1-mar)
4. [2 Mar](#2-mar)
5. [3 Mar](#3-mar)
6. [4 Mar](#4-mar)
7. [7 Mar](#7-mar)

### 25 Feb

Topic learn on this day:

1. Flowchart
2. OOP
3. Inheritance

### 28 Feb

Topic learn on this day:

1. For loop
2. While loop
3. Do while loop
4. Recursion
5. Sorting algorithm (bubble sort, merge sort, quick sort, binary search tree)

### 1 Mar

Topic learn on this day:

1. For loop
2. Bubble sort
3. Merge sort

### 2 Mar

Topic learn on this day:

1. Quick sort

### 3 Mar

Topics learn on this day:

1. Binary search tree

### 4 Mar

Topics learn on this day:

1. Pattern and Fibonacci
2. OOP (Inheritance)
3. Function, Class, Method

### 4 Mar

Topics learn on this day:

1. Encapsulation (private, public, protected)
2. Encryption and Decryption
3. Abstractions
4. Polymorphism
